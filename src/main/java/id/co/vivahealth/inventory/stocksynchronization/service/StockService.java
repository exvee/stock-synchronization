package id.co.vivahealth.inventory.stocksynchronization.service;


import id.co.vivahealth.inventory.stocksynchronization.domain.Stock;

import java.util.List;

public interface StockService {
    void updateAllByBatch(List<Stock> stocks);
    void deleteByStore(String store);
    //void updateMirrorDatabase(String store);
}
