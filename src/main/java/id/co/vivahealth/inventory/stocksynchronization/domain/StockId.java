package id.co.vivahealth.inventory.stocksynchronization.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockId implements Serializable {
    private static final long serialVersionUID = 1L;

    private String store;
    private String sku;
}
