package id.co.vivahealth.inventory.stocksynchronization.repositories;

import id.co.vivahealth.inventory.stocksynchronization.domain.Stock;
import id.co.vivahealth.inventory.stocksynchronization.domain.StockId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends CrudRepository<Stock, StockId> {
    void deleteByStore(String store);
    //void updateAllByBatch();
    //void deleteByStoreAndSku(String store, String sku);
    //long deleteByStore

    //@Modifying
    //@Query(value = "UPDATE Store_Transaction.dbo.T_Stock_Store set StockS_Qty = Qty, StockS_LastUpdate = getdate(), stocks_lastupdateserver=getdate() FROM EA_INV_SERVICE.dbo.stock WHERE sku = StockS_ProdID and store = StockS_StoreID and store = ?1 ",
    //        nativeQuery = true)
    //void updateMirrorDatabase(String store);
}
