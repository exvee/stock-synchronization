package id.co.vivahealth.inventory.stocksynchronization.service.impl;

import id.co.vivahealth.inventory.stocksynchronization.domain.Stock;
import id.co.vivahealth.inventory.stocksynchronization.repositories.StockRepository;
import id.co.vivahealth.inventory.stocksynchronization.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class StockServiceImpl implements StockService {

    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    private int batchSize;

    @Autowired
    private StockRepository stockRepository;

    //@Transactional
    //public boolean updateAll(List<Stock> stocks) {
    //        stockRepository.saveAll(stocks);
    //        return true;
    //}

    @Override
    @Transactional
    public void updateAllByBatch(List<Stock> stocks) {
        int size = stocks.size();
        int counter = 0;

        List<Stock> temp = new ArrayList<>();

        //stockRepository.deleteByStore(stocks.get(0).getStore());

        for (Stock s : stocks) {
            temp.add(s);

            //stockRepository.deleteByStoreAndSku(s.getStore(), s.getSku());

            if ((counter + 1) % batchSize == 0 || (counter + 1) == size) {
                stockRepository.saveAll(temp);
                temp.clear();
            }

            counter++;
        }
    }

    @Override
    @Transactional
    public void deleteByStore(String store) {
        stockRepository.deleteByStore(store);
    }

    //@Transactional(readOnly = true)
    //public List<Stock> getAllStocks() {
    //   return stockRepository.findAll();
    //}

    //public void updateMirrorDatabase(String storeId) {
    //    stockRepository.updateMirrorDatabase(storeId);
    //}
}
