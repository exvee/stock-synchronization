package id.co.vivahealth.inventory.stocksynchronization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class StockSynchronizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockSynchronizationApplication.class, args);
	}

}
