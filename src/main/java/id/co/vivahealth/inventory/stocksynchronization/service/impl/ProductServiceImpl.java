package id.co.vivahealth.inventory.stocksynchronization.service.impl;

import id.co.vivahealth.inventory.stocksynchronization.domain.Product;
import id.co.vivahealth.inventory.stocksynchronization.repositories.ProductRepository;
import id.co.vivahealth.inventory.stocksynchronization.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Optional<Product> find(Long id) {
        return repository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return repository.findAll();
    }
}
