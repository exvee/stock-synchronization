package id.co.vivahealth.inventory.stocksynchronization.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product extends Base{

    @Column(name="sku", unique=true,length = 8, nullable = false)
    private String productId;

    @Column(name="long_name", length = 150, nullable = false)
    private String productName;

    @Column(name="short_name", length = 150, nullable = false)
    private String shortName;

    @Column(nullable = false)
    private Boolean isNationalPrice;

    @Column(nullable = false)
    private BigDecimal nationalPrice;
}
