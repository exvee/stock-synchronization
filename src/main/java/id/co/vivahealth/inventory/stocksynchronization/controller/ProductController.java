package id.co.vivahealth.inventory.stocksynchronization.controller;

import id.co.vivahealth.inventory.stocksynchronization.domain.Product;
import id.co.vivahealth.inventory.stocksynchronization.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/inventory")
public class ProductController {

    @Autowired
    private ProductServiceImpl service;

    @GetMapping("/{id}")
    public Product read(@PathVariable Long id) {
        return service.find(id).get();
    }

    @GetMapping("/all")
    public List<Product> readAll() {
        return service.findAll();
    }

}
