package id.co.vivahealth.inventory.stocksynchronization.repositories;

import id.co.vivahealth.inventory.stocksynchronization.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
