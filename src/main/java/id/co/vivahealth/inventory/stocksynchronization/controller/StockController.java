package id.co.vivahealth.inventory.stocksynchronization.controller;

import id.co.vivahealth.inventory.stocksynchronization.domain.ListStockDAO;
import id.co.vivahealth.inventory.stocksynchronization.domain.Stock;
import id.co.vivahealth.inventory.stocksynchronization.service.impl.StockServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/inventory")
public class StockController {

    //@Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    //private int batchSize;

    @Autowired
    private StockServiceImpl stockService;

    @PostMapping("/{storeId}/upload")
    public ResponseEntity<Void> updateStock(@PathVariable String storeId, @RequestBody ListStockDAO data) {


        log.info("Processing Inventory Start : " + storeId);
        stockService.deleteByStore(storeId);


        List<Stock> stocks = data.getData().parallelStream()
                .map(stock -> new Stock(storeId, stock.getSku(), stock.getQty()))
                //.map(stock -> new Stock(storeId ) )
                .collect(Collectors.toList());


        stockService.updateAllByBatch(stocks);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
        //int size = stocks.size();
        //int counter = 0;

        /*List<Stock> temp = new ArrayList<>();

        //start batching here

        for (Stock s : stocks) {
            temp.add(s);

            if ((counter + 1) % batchSize == 0 || (counter + 1) == size) {
                stockService.updateAll(temp);
                temp.clear();
            }

            counter++;
        }
        //end batching

        //log.info("Processing Inventory DAO : " + data.getData());

        //for(Stock s: stocks){
        //    log.info("Processing Inventory Detail : " + s.getStore() + " " + s.getSku() + " " + s.getQty());
        //}

        //stockService.updateAll(stocks);

        //push to message broker
        //for (Stock stock: stocks) {
        //    rabbitMQSenderService.send(stock);
        //}
        //end push

        return  ResponseEntity.ok(data);*/
    //}

}
