package id.co.vivahealth.inventory.stocksynchronization.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stock",
        uniqueConstraints = @UniqueConstraint(columnNames={"store", "sku"})
      )
//@IdClass(StockId.class)
//public class Stock implements Serializable {
public class Stock extends Base{
   // private static final long serialVersionUID = 1L;

    //@Id
    @Column(length = 4)
    @Size(min = 4, max = 4)
    @NotNull
    private String store;

    //@Id
    @JsonProperty("Sku")
    @Column(length = 8)
    @Size(min = 8, max = 8)
    @NotNull
    private String sku;

    @JsonProperty("Qty")
    @NotNull
    private double qty;

}
