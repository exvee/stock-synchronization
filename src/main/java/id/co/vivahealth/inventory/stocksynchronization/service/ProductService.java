package id.co.vivahealth.inventory.stocksynchronization.service;


import id.co.vivahealth.inventory.stocksynchronization.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Optional<Product> find(Long id);
    List<Product> findAll();
}
